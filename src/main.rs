use persy::{ByteVec, Config, Persy, PersyError, TransactionConfig, ValueMode};

use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use tracing::{info, trace};
use tracing_subscriber::prelude::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    /*
    let tracer = opentelemetry_jaeger::new_agent_pipeline()
        .with_endpoint("127.0.0.1:14268")
        .with_service_name("persy_simple_kv_perf_remove")
        .install_simple()?;
        */
    opentelemetry::global::set_text_map_propagator(
        opentelemetry::sdk::propagation::TraceContextPropagator::new(),
    );
    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(opentelemetry_otlp::new_exporter().http())
        .install_simple()?;
    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    let fmt_layer = tracing_subscriber::fmt::layer().with_target(false);
    let filter_layer = tracing_subscriber::EnvFilter::try_from_default_env()
        .or_else(|_| tracing_subscriber::EnvFilter::try_new("info"))
        .unwrap();

    tracing_subscriber::registry()
        .with(opentelemetry)
        .with(filter_layer)
        .with(fmt_layer)
        .try_init()?;

    let mut config = Config::new();
    config.change_cache_size(1024 * 1024 * 256);
    let persy = Persy::open_or_create_with("./loaded_data.persy", config, |_p| Ok(()))?;

    let finished = Arc::new(AtomicUsize::new(10));
    let atomic = Arc::new(AtomicUsize::new(0));
    if !persy.exists_index("test")? {
        let mut tx = persy.begin()?;

        tx.create_index::<ByteVec, ByteVec>("test", ValueMode::Replace)?;

        let prepared = tx.prepare()?;

        prepared.commit()?;
    }

    let reader = atomic.clone();
    let fin = finished.clone();
    std::thread::spawn(move || loop {
        std::thread::sleep(std::time::Duration::from_millis(1000));

        reader.store(0, Ordering::SeqCst);
        if fin.load(Ordering::SeqCst) == 0 {
            break;
        }
    });

    loop {
        let mut handles: Vec<std::thread::JoinHandle<Result<(), PersyError>>> = vec![];
        for t in 0..30 {
            let db = persy.clone();
            let atomic_cloned = atomic.clone();
            let fin = finished.clone();
            handles.push(
                std::thread::Builder::new()
                    .spawn(move || {
                        info!(" started {:?}", std::thread::current().id());
                        let mut tx = db
                            .begin_with(TransactionConfig::new())
                            .map_err(|e| e.error())?;
                        for i in 0..100 {
                            trace!("iter {}", i);
                            for n in 0..1000 {
                                let kv = ByteVec::from(format!("{}+{}", t, n).as_bytes());
                                tx.put("test", kv.clone(), kv).map_err(|e| e.error())?;
                                if n % 100 == 0 {
                                    let prepared = tx.prepare().map_err(|e| e.error())?;
                                    prepared.commit().map_err(|e| e.error())?;
                                    tx = db
                                        .begin_with(TransactionConfig::new())
                                        .map_err(|e| e.error())?;
                                }
                                atomic_cloned.fetch_add(1, Ordering::SeqCst);
                            }

                            for n in 0..1000 {
                                let kv = ByteVec::from(format!("{}+{}", t, n).as_bytes());
                                tx.remove("test", kv.clone(), Some(kv))
                                    .map_err(|e| e.error())?;
                                if n % 100 == 0 {
                                    let prepared = tx.prepare().map_err(|e| e.error())?;
                                    prepared.commit().map_err(|e| e.error())?;
                                    tx = db
                                        .begin_with(TransactionConfig::new())
                                        .map_err(|e| e.error())?;
                                }
                                atomic_cloned.fetch_add(1, Ordering::SeqCst);
                            }
                        }
                        info!(" finished {:?}", std::thread::current().id());
                        // use persy::inspect::PersyInspect;
                        //db.inspect_tree::<ByteVec, ByteVec,_>("test",&mut persy::debug::PrintTreeInspector::new() ).map_err(|e|e.error())?;
                        fin.fetch_sub(1, Ordering::SeqCst);

                        Ok(())
                    })
                    .unwrap(),
            )
        }

        for t in handles {
            t.join().unwrap()?;
        }
        let mut input = String::new();
        std::io::stdin().read_line(&mut input)?;
        if input.starts_with("n") {
            break;
        }
    }

    Ok(())
}
